import numpy as np
import matplotlib.pyplot as plt

import ufl
import dolfin as df
import dolfin_adjoint as da

df.parameters["form_compiler"]["cpp_optimize"] = True
df.parameters["form_compiler"]["representation"] = "uflacs"
df.parameters["form_compiler"]["quadrature_degree"] = 4
df.set_log_level(50)


class NonlinearProblem(df.NonlinearProblem):
    def __init__(self, J, F, bcs):
        self.bilinear_form = J
        self.linear_form = F
        self.bcs = bcs
        df.NonlinearProblem.__init__(self)

    def F(self, b, x):
        da.assemble(self.linear_form, tensor=b)
        self.bcs.apply(b, x)

    def J(self, A, x):
        da.assemble(self.bilinear_form, tensor=A)
        self.bcs.apply(A)

def def_state_space(mesh):
    P2 = df.VectorElement("Lagrange", mesh.ufl_cell(), 2)
    P1 = df.FiniteElement("Lagrange", mesh.ufl_cell(), 1)
    TH = df.FunctionSpace(mesh, df.MixedElement([P2, P1]))

    return TH


def cond(a):
    return ufl.conditional(a > 0, a, 0)


def psi_holzapfel(
    IIFx,
    I4e1,
    a=da.Constant(1),
    b=da.Constant(5),
    af=da.Constant(1),
    bf=da.Constant(5),
):

    W_hat = a / (2 * b) * (df.exp(b * (IIFx - 3)) - 1)
    W_f = af / (2 * bf) * (df.exp(bf * cond(I4e1 - 1) ** 2) - 1)

    return W_hat + W_f


def fiber_direction(theta):
    R = df.as_matrix(
        (
            (df.cos(theta), -df.sin(theta), 0),
            (df.sin(theta), df.cos(theta), 0),
            (0, 0, 1),
        )
    )

    return R * df.as_vector([1.0, 0.0, 0.0])


def PK_stress_tensor(F, p, active_fun, psi):
    theta = da.Constant(0)
    f0 = fiber_direction(theta)

    C = F.T * F
    J = df.det(F)
    Jm23 = pow(J, -float(2) / 3)

    I1 = Jm23 * df.tr(C)
    I4f = Jm23 * df.inner(C * f0, f0)

    mgamma = 1 - active_fun
    I1e = mgamma * I1 + (1 / mgamma ** 2 - mgamma) * I4f
    I4fe = 1 / mgamma ** 2 * I4f

    PK1 = df.diff(psi(I1e, I4fe), F) + p * Jm23 * J * df.inv(F.T)

    return PK1


def elasticity_term(PK1, v):
    return df.inner(PK1, df.grad(v)) * df.dx


def pressure_term(J, q):
    return q * (J - 1) * df.dx


def def_weak_form(
    mesh,
    active_fun,
):
    TH = def_state_space(mesh)
    V, _ = TH.split()

    xmin_bnd = "on_boundary && near(x[0], 0)"
    bcs = da.DirichletBC(V, df.Constant(np.zeros(3)), xmin_bnd)

    state = da.Function(TH)
    u, p = df.split(state)

    F = df.variable(df.Identity(3) + df.grad(u))
    J = df.det(F)

    PK1 = PK_stress_tensor(F, p, active_fun, psi_holzapfel)

    v, q = df.split(df.TestFunction(TH))
    R = elasticity_term(PK1, v) + pressure_term(J, q)

    return R, state, bcs


def cost_function(u_model, u_data, mesh):
    norm = lambda f: da.assemble(df.inner(f, f) * df.dx(mesh))
    return norm(u_model - u_data) / norm(da.Constant(1))


def create_forward_problem(mesh, active_fun):
    R, state, bcs = def_weak_form(
        mesh,
        active_fun,
    )
    
    jacobian = df.fem.formmanipulations.derivative(R, state)
    problem = NonlinearProblem(jacobian, R, bcs)
    
    solver = da.NewtonSolver()
    solver.solve(problem, state.vector())
    
    u, _ = state.split()

    return u


def simple_test_active():
    N = 6
    mesh = da.UnitCubeMesh(N, N, N)

    active = da.Constant(0.05)
    u_synthetic = create_forward_problem(mesh, active)

    # inverse problem

    active_ctrl = da.Constant(0.0)

    u_model = create_forward_problem(mesh, active_ctrl)

    J = cost_function(
        u_model,
        u_synthetic,
        mesh,
    )

    tracked_values = []

    controls = da.Control(active_ctrl)
    reduced_functional = da.ReducedFunctional(J, controls)

    problem = da.MinimizationProblem(reduced_functional, bounds=[(0, 0.3)])
    solver = da.IPOPTSolver(problem)

    opt_values = solver.solve()
    print("Estimated active strain: ", float(opt_values))


simple_test_active()
