import numpy as np
import dolfin as df
import dolfin_adjoint as da

from mpsadjoint import (
    solve_inverse_problem,
    define_state_space,
    define_bcs,
    define_weak_form,
    define_char_functions,
    set_fenics_parameters,
    write_active_strain_to_file,
    write_fiber_angle_to_file,
    write_fiber_direction_to_file,
    write_displacement_to_file,
)
from mpsadjoint.cardiac_mechanics import solve_forward_problem_iteratively 
from mpsadjoint.nonlinearproblem import NonlinearProblem

def noise(fun_space, sigma, seed):
    r"""

    Generates a function with a normal distribution $N[0, \sigma]$.

    Args:
        fun_space - a (Vector)FunctionSpace; function space of the same
            dimension as the function to perturbate live in
        sigma - a float; variation in noise to add

    Returns:
        a function in V filled with normally distributed values

    """
    noise_fun = df.Function(fun_space)
    shape = noise_fun.vector()[:].shape

    noise_fun.vector()[:] = np.random.uniform(-sigma, sigma, size=shape)

    return noise_fun



def generate_synthetic_data(geometry, active_strain, theta):
    set_fenics_parameters()

    mesh = geometry.mesh
    dirichlet_surfaces = geometry.dirichlet_surfaces

    TH = define_state_space(mesh)
    V = df.VectorFunctionSpace(mesh, "CG", 1)
    U = df.FunctionSpace(mesh, "CG", 1)
    bcs = define_bcs(dirichlet_surfaces, TH)

    active = df.Function(U)
    active.vector()[:] = 0
    
    theta_fun = df.Function(U)
    theta_fun.assign(df.project(theta, U))
    
    xi_tissue, xi_pillars = define_char_functions(geometry)


    R, state = define_weak_form(
        TH,
        active,
        theta_fun,
        xi_tissue,
        xi_pillars,
    )

    problem = NonlinearProblem(R, state, bcs)
    solver = df.NewtonSolver()
    solver.parameters.update(
        {
            "relative_tolerance": 1e-5,
            "absolute_tolerance": 1e-5,
        }
    )



    u_synthetic = []

    for i in range(len(active_strain)):
        print(f"Iterative step: {i} / {len(active_strain)}")
        active_values = df.project(active_strain[i], U)
    
        solve_forward_problem_iteratively(
            active,
            theta_fun,
            state,
            solver,
            problem,
            active_values,
            theta_fun,
            step_length=1.0,
        )

        _u, _ = state.split()
        u_synthetic.append(df.project(_u, V))

    return u_synthetic


def inverse_crime(
        geometry,
        active,
        theta,
        output_folder,
        num_iterations_iterative,
        num_iterations_combined,
        reg_factor,
        noise_level=0,
):

    # two-face iteration: First save values, then run the inversion
    #if os.path.isfolder(output_folder):

    u_synthetic = generate_synthetic_data(geometry, active, theta)

    np.random.seed(1)
    
    U = df.FunctionSpace(geometry.mesh, "CG", 1)
    V = df.VectorFunctionSpace(geometry.mesh, "CG", 1)
    u_data = []

    for u in u_synthetic:

        noise_uniform1 = noise(U, noise_level, 1)
        noise_uniform2 = noise(U, noise_level, 2)
        noise_uniform3 = noise(U, noise_level, 3)

        u1 = df.project(u, V)
        u_d = da.Function(V)
        values = u_d.vector()[:]
    
        values[::3] = (1 + noise_uniform1.vector()[:])*u1.vector()[::3]
        values[1::3] = (1 + noise_uniform2.vector()[:])*u1.vector()[1::3]
        #values[2::3] = (1 + noise_uniform3.vector()[:])*u1.vector()[2::3]         # not really important
        u_d.vector()[:] = values

        u_data.append(u_d)        

    write_original_files(output_folder, geometry.mesh, active, theta, u_data)
        
    active_m, theta_m, _ = solve_inverse_problem(
        geometry,
        u_data,
        reg_factor,
        output_folder,
        num_iterations_iterative,
        num_iterations_combined,
    )


def write_original_files(output_folder, mesh, active, theta, u_data):
    U = df.FunctionSpace(mesh, "CG", 1)
    V = df.VectorFunctionSpace(mesh, "CG", 1)

    filename_active = f"{output_folder}/active_strain_original.xdmf"
    write_active_strain_to_file(U, active, filename_active)

    filename_theta = f"{output_folder}/theta_original.xdmf"
    write_fiber_angle_to_file(U, theta, filename_theta)

    filename_fiber_dir = f"{output_folder}/fiber_direction_original.xdmf"
    write_fiber_direction_to_file(V, theta, filename_fiber_dir)

    filename_displacement = f"{output_folder}/displacement_original.xdmf"
    write_displacement_to_file(V, u_data, filename_displacement)

