"""

Åshild Telle / Simula Research Laboratory / 2021

"""


import numpy as np
import dolfin as df
import dolfin_adjoint as da

from scipy.interpolate import RegularGridInterpolator


def mps_to_fenics(mps_data, mps_info, mesh, time_start, time_stop):
    V = df.VectorFunctionSpace(mesh, "CG", 1)
    v_d = df.vertex_to_dof_map(V)

    u_values = []

    xcoords, ycoords = define_value_ranges(mps_data, mps_info)

    mesh_xcoords = mesh.coordinates()[:, 0]
    mesh_ycoords = mesh.coordinates()[:, 1]
    mesh_zcoords = mesh.coordinates()[:, 2]

    for t in range(time_start, time_stop):
        u = da.Function(V, name=r"Displacement BF data ($\mu m$)")

        ip_fun = mps_interpolation(
            mps_data[t],
            xcoords,
            ycoords,
        )

        xvalues, yvalues, zvalues = ip_fun(mesh_xcoords, mesh_ycoords, mesh_zcoords)

        u.vector()[v_d] = np.array((xvalues, yvalues, zvalues)).transpose().flatten()

        u_values.append(u)

    return u_values


def define_value_ranges(mps_data, mps_info):

    um_per_pixel = mps_info["um_per_pixel"]

    _, X, Y, _ = mps_data.shape

    xmax = um_per_pixel / 2 + um_per_pixel * X
    ymax = um_per_pixel / 2 + um_per_pixel * Y

    print(xmax, ymax)

    xvalues = np.linspace(um_per_pixel / 2, xmax, X)
    yvalues = np.linspace(um_per_pixel / 2, ymax, Y)

    return xvalues, yvalues


def mps_interpolation(mps_data, xvalues, yvalues):
    ip_x = RegularGridInterpolator(
        (xvalues, yvalues),
        mps_data[:, :, 1],
        bounds_error=False,
        fill_value=0,
    )

    ip_y = RegularGridInterpolator(
        (xvalues, yvalues),
        mps_data[:, :, 0],
        bounds_error=False,
        fill_value=0,
    )

    zeros = np.zeros_like(mps_data[:, :, 0])
    ip_z = RegularGridInterpolator(
        (xvalues, yvalues), zeros, bounds_error=False, fill_value=0
    )

    ip_fun = lambda x, y, z: np.array((ip_x((x, y)), ip_y((x, y)), ip_z((x, y))))

    return ip_fun
