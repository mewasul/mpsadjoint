"""

Loads mesh + calculates "top layer" of the mesh, which is always the surface used
for comparison with BF data during experiments.

"""

import numpy as np
import os
import dolfin as df
import dolfin_adjoint as da
from mpi4py import MPI
from pathlib import Path
from collections import namedtuple

Geometry = namedtuple("Geometry", ["mesh", "pillars", "dirichlet_surfaces", "ceiling"])

def load_mesh_h5(filename: os.PathLike, save_pvd_file: bool = False) -> Geometry:

    comm = MPI.COMM_WORLD
    filename = Path(filename)
    if not filename.is_file():
        raise FileNotFoundError(f"File {filename} does not exist")
    if filename.suffix != ".h5":
        raise RuntimeError("File {filename} is not an HDF5 file")

    mesh = da.Mesh()
    with df.HDF5File(comm, filename.as_posix(), "r") as h5_file:
        h5_file.read(mesh, "mesh", False)

        dirichlet_surfaces = df.MeshFunction("size_t", mesh, 2, 0)
        pillars = df.MeshFunction("size_t", mesh, 3, 0)

        h5_file.read(dirichlet_surfaces, "surfaces")
        h5_file.read(pillars, "volumes")

    # TODO
    # hack, needs to be in mesh processing script!
    pa = pillars.array()[:]
    pillars.array()[:] = np.where(pa==12, 0, 1)

    nodes = mesh.num_vertices()
    cells = mesh.num_cells()

    print(f"Number of nodes: {nodes}, number of elements: {cells}")

    ceiling = find_ceiling(mesh)

    if save_pvd_file:
        df.File("dirichlet_surfaces.pvd") << dirichlet_surfaces
        df.File("ceiling.pvd") << ceiling

    return Geometry(mesh=mesh, dirichlet_surfaces=dirichlet_surfaces, ceiling=ceiling, pillars=pillars)


def find_ceiling(mesh: da.Mesh) -> df.MeshFunction:

    surface = df.MeshFunction("size_t", mesh, 2)

    zmax = max(mesh.coordinates()[:, 2])

    ceiling = df.CompiledSubDomain(f"near(x[2], {zmax}, 1e-5) && on_boundary")
    ceiling.mark(surface, 2)

    return surface
