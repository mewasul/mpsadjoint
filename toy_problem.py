import os
import dolfin as df
import dolfin_adjoint as da
from mpi4py import MPI

from mpsadjoint import (
    MaterialModel,
    load_mesh,
    InverseProblem,
    InverseProblem2,
)

def check_equanitly_L2(inverse_fun, true_fun, dx):
    eps = 1E-6
    assert da.assemble(((inverse_fun - true_fun)**2)*dx(1)) < eps


def inverse_constant(method, experiment, neumann):

    assert method in ["L-BFGS-B", "IPOPT", "Newton-CG"], "Error: Unknown method"
    assert experiment in ["Constant", "ConstantCG1", "Sine"], "Error: Unknown experiment."
    assert isinstance(neumann, bool), "Error: Neumann needs to be boolean."

    # load mesh

    mesh_file = os.path.join("meshes", "mesh_unittests.xdmf")
    mesh, volumes, facets = load_mesh(mesh_file)
    
    #U = df.FunctionSpace(mesh, "CG", 1)
    
    # load previously calculated solution

    V = df.VectorFunctionSpace(mesh, "CG", 2)
    u_saved = da.Function(V)

    if experiment in ["Constant", "ConstantCG1"]:
        filename = "forward_solutions/forward_solution_constant.h5"
    else:
        filename = "forward_solutions/forward_solution_sine.h5"

    sol_file = df.HDF5File(MPI.COMM_WORLD, filename, "r")
    sol_file.read(u_saved, "/forward")
    sol_file.close()
    
    # inverse crime

    inv_problem = InverseProblem(mesh, volumes, facets, u_saved)
    inv_problem.solve(0, method)

    active_str = inv_problem.active_str
    neumann = inv_problem.neumann

    dx = df.Measure("dx", domain=mesh, subdomain_data=volumes)

    


def inverse_sine(method):

    # load mesh

    mesh_file = os.path.join("meshes", "mesh_unittests_sine.xdmf")
    mesh, volumes, facets = load_mesh(mesh_file)
    
    CG = df.FunctionSpace(mesh, "CG", 1)
    f = df.Expression("0.2*(1 + sin(5*3.14/800*x[0]))", element = CG.ufl_element())
    true_active_str = da.interpolate(f, CG)
    
    # load previously calculated solution

    V = df.VectorFunctionSpace(mesh, "CG", 2)
    u_saved = da.Function(V)

    filename = os.path.join(
        os.path.dirname(__file__), "forward_spatial_sine.h5"
    )
    sol_file = df.HDF5File(MPI.COMM_WORLD, filename, "r")
    sol_file.read(u_saved, "/forward")
    sol_file.close()
    
    # inverse crime
    
    eps = 1E-6

    inv_problem = InverseProblem(mesh, volumes, u_saved)
    active_str = inv_problem.solve(0, method)


if __name__ == "__main__":
    inverse_constant("L-BFGS-B")
    #inverse_sine()
