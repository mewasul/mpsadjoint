#!/usr/bin/env python3
"""

This MWE displays how one can simulate uniaxial stretching, considering a simple homogeneous unit cube
representing a tissue block (3D), using Dirichlet boundary conditions.

Åshild Telle / Simula Research Laboratory / 2021

"""

import os
import numpy as np
import dolfin as df
import ufl
import matplotlib.pyplot as plt
from functools import partial

df.parameters["form_compiler"]["cpp_optimize"] = True
df.parameters["form_compiler"]["representation"] = "uflacs"
df.parameters["form_compiler"]["quadrature_degree"] = 4


def psi_holzapfel(F, a, b, a_f, b_f):
    e1 = df.as_vector([1.0, 0.0, 0.0])

    J = df.det(F)
    C = pow(J, -float(2) / 3) * F.T * F

    IIFx = df.tr(C)
    I4e1 = df.inner(C * e1, e1)

    W_hat = a / (2 * b) * (df.exp(b * (IIFx - 3)) - 1)
    W_f = a_f / (2 * b_f) * (df.exp(b_f * cond(I4e1 - 1) ** 2) - 1)

    return W_hat + W_f


def cond(a):
    return ufl.conditional(a > 0, a, 0)


def define_weak_form(mesh, strain_energy_fun):

    P1 = df.FiniteElement("Lagrange", mesh.ufl_cell(), 1)
    P2 = df.VectorElement("Lagrange", mesh.ufl_cell(), 2)

    state_space = df.FunctionSpace(mesh, df.MixedElement([P1, P2]))

    state = df.Function(state_space, name="state")
    test_state = df.TestFunction(state_space)

    p, u = df.split(state)
    q, v = df.split(test_state)

    # Kinematics
    d = len(u)
    I = df.Identity(d)  # Identity tensor
    F = df.variable(I + df.grad(u))  # Deformation gradient
    J = df.det(F)

    psi = strain_energy_fun(F)
    P = df.diff(psi, F) + p * J * df.inv(F.T)

    surface_normal = df.FacetNormal(mesh)

    load = df.inner(P * surface_normal, surface_normal)

    # Weak form
    R = 0
    R += elasticity_term(P, v)
    R += pressure_term(q, J)

    V = state_space.split()[1]

    return R, state, u, V, load


def elasticity_term(P, v):
    return df.inner(P, df.grad(v)) * df.dx


def pressure_term(q, J):
    return q * (J - 1) * df.dx


def evaluate_load(load, ds):

    total_load = df.assemble(load * ds)
    #area = df.assemble(
    #    df.det(F) * df.inner(df.inv(F).T * surface_normal, surface_normal) * ds
    #)

    total_load *= 1E-12     # load = total_load/area in µm * area in m
    total_load *= 1E3      # kN -> N
    total_load *= 1E6     # N -> µN

    return total_load # µN

def define_ds(mesh, max_x, idt):
    surface = df.MeshFunction("size_t", mesh, 2)

    class Wall(df.SubDomain):
        def inside(self, x, on_boundary):
            return df.near(x[0], max_x, eps=1e-5) and on_boundary

    w = Wall()
    w.mark(surface, idt)

    ds = df.Measure("ds", domain=mesh, subdomain_data=surface)

    return ds


filename = "meshes/chip_wo_pillars_2.xml"
mesh = df.Mesh(filename)

# just for plotting purposes
#disp_file = df.XDMFFile("u_chip.xdmf")
#V_CG2 = df.VectorFunctionSpace(mesh, "CG", 2)
#u_fun = df.Function(V_CG2, name="Displacement")

a = df.Constant(1)
b = df.Constant(5)
a_f = df.Constant(1)
b_f = df.Constant(5)

psi1 = partial(psi_holzapfel, a=a, b=b, a_f=a_f, b_f=b_f)

a = df.Constant(2.28)
b = df.Constant(9.726)
a_f = df.Constant(1.685)
b_f = df.Constant(15.779)

psi2 = partial(psi_holzapfel, a=a, b=b, a_f=a_f, b_f=b_f)

for (psi, label) in zip([psi1, psi2], ["Arbitary", "Holzapfel"]):

    min_x = min(mesh.coordinates()[:, 0])
    max_x = max(mesh.coordinates()[:, 0])
    min_y = min(mesh.coordinates()[:, 1])
    min_z = min(mesh.coordinates()[:, 2])

    stretch = np.linspace(0, 0.2, 20)

    stretch_fun = df.Constant(0)

    R, state, u, V, load = define_weak_form(mesh, psi)

    # define bcs
    xmin_bnd = f"on_boundary && near(x[0], {min_x}, 1E-5)"
    xmax_bnd = f"on_boundary && near(x[0], {max_x}, 1E-5)"
    zmin_bnd = f"on_boundary && near(x[2], {min_z}, 1E-5)"
    fixed_pt = f"near(x[0], {min_x}) && near(x[1], {min_y}) && near(x[2], {min_z})"

    idt = 1
    ds = define_ds(mesh, max_x, idt)

    bcs = [
        df.DirichletBC(V.sub(0), df.Constant(0), xmin_bnd),  # keep fixed in x coord
        df.DirichletBC(V, np.array([0, 0, 0]), fixed_pt, method="pointwise"),
        df.DirichletBC(V.sub(2), df.Constant(0), zmin_bnd),  # keep fixed in z coord
        df.DirichletBC(V.sub(0), stretch_fun, xmax_bnd),  # move this in x coord
    ]

    load_values = []

    for s in stretch:
        stretch_fun.assign(s*(max_x - min_x))
        df.solve(R == 0, state, bcs=bcs)

        l = evaluate_load(load, ds(idt))
        load_values.append(l)
    
        #if label=="Holzapfel":
        # plotting again
        #    u_fun.assign(df.project(u, V_CG2))
        #    disp_file.write_checkpoint(u_fun, "Displacement (µm)", s, append=True)

    load_values = np.array(load_values)
    plt.plot(load_values, label=label)

#disp_file.close()

plt.ylabel("Load*area (µN)")
plt.xlabel("Stretch (%)")
#plt.ylim(0, 1500)
plt.legend()
plt.savefig("strain_energy_functions_different_scale.png")
plt.show()
