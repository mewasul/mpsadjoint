#!/bin/sh

# all paper experiments related to synthetic data
# runs in one batch (on a normal computer with 8 cores)

#python synthetic.py &

#python synthetic.py --active "constant" &
#python synthetic.py --fiber "constant" &
#python synthetic.py --active "constant" --fiber "constant" &

for noise in 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0
do
	python synthetic.py --noise_level $noise &
	python synthetic.py --noise_level $noise --fiber constant &
done
