import dolfin
from pathlib import Path
import numpy as np
import pytest
from mpsadjoint import mesh_setup


def test_load_mesh_h5():
    mesh = dolfin.UnitCubeMesh(2, 2, 2)
    volumes = dolfin.MeshFunction("size_t", mesh, 3, 0)
    volumes.array()[:] = 1

    surfaces = dolfin.MeshFunction("size_t", mesh, 2, 0)
    surfaces.array()[:] = 42

    filename = Path("test_file.h5")
    if filename.is_file():
        filename.unlink()

    with dolfin.HDF5File(mesh.mpi_comm(), filename.as_posix(), "w") as h5file:
        h5file.write(mesh, "mesh")
        h5file.write(surfaces, "surfaces")
        h5file.write(volumes, "volumes")

    geometry = mesh_setup.load_mesh_h5(filename)

    assert (geometry.mesh.coordinates() == mesh.coordinates()).all()
    assert (geometry.dirichlet_surfaces.array() == surfaces.array()).all()
    assert (geometry.pillars.array() == volumes.array()).all()

    filename.unlink()


def test_load_mesh_h5_raises_FileNotFoundError_when_file_does_not_exist():
    with pytest.raises(FileNotFoundError):
        mesh_setup.load_mesh_h5("no_exist.h5")


def test_load_mesh_h5_raises_RunTimeError_on_wrong_extension():
    f = Path("no_exist.xml")
    f.touch()
    with pytest.raises(RuntimeError):
        mesh_setup.load_mesh_h5(f)
    f.unlink()


if __name__ == "__main__":
    test_load_mesh_h5()
