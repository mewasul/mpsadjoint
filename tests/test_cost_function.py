import math
import dolfin_adjoint as da
import dolfin as df
import pytest
import mpsadjoint

from mpsadjoint.mesh_setup import Geometry, find_ceiling


def test_spatial_reg_constant_control_CG1():
    mesh = da.UnitCubeMesh(3, 3, 3)
    U = df.FunctionSpace(mesh, "CG", 1)
    f = da.Function(U)
    f.assign(da.Constant(0.1))
    reg = mpsadjoint.inverse.spatial_reg(mesh, controls=[f])
    assert math.isclose(reg, 0)


def test_spatial_reg_linear_Z_CG1():
    mesh = da.UnitCubeMesh(3, 3, 3)
    U = df.FunctionSpace(mesh, "CG", 1)
    f = da.interpolate(da.Expression("x[2]", degree=1), U)
    reg = mpsadjoint.inverse.spatial_reg(mesh, controls=[f])

    assert math.isclose(reg, 1)


def test_spatial_reg_linear_X_CG1():
    mesh = da.UnitCubeMesh(3, 3, 3)
    U = df.FunctionSpace(mesh, "CG", 1)
    f = da.interpolate(da.Expression("x[0]", degree=1), U)
    reg = mpsadjoint.inverse.spatial_reg(mesh, controls=[f])

    assert math.isclose(reg, 0)


@pytest.mark.parametrize(
    "u_model_arr, expected_cost_diff",
    [
        ((0.0, 0.0, 0.0), 0.0),
        ((0.0, 0.0, 1.0), 0.0),
        ((1.0, 1.0, 1.0), 2.0),
    ],
)
def test_cost_diff(u_model_arr, expected_cost_diff):
    mesh = da.UnitCubeMesh(3, 3, 3)
    V = df.VectorFunctionSpace(mesh, "CG", 1)
    u_model = da.Function(V)
    u_data = da.Function(V)
    u_model.assign(da.Constant(u_model_arr))
    u_data.assign(da.Constant((0.0, 0.0, 0.0)))

    ceiling = find_ceiling(mesh)
    geometry = Geometry(
        mesh=mesh, pillars=None, dirichlet_surfaces=None, ceiling=ceiling
    )
    cost_diff = mpsadjoint.inverse.cost_diff(geometry, [u_model], [u_data])

    assert math.isclose(cost_diff, expected_cost_diff)


def test_cost_function():
    mesh = da.UnitCubeMesh(3, 3, 3)
    U = df.FunctionSpace(mesh, "CG", 1)
    V = df.VectorFunctionSpace(mesh, "CG", 1)
    u_model = da.Function(V)
    u_data = da.Function(V)
    controls = [da.Function(U)]

    ceiling = find_ceiling(mesh)
    geometry = Geometry(
        mesh=mesh, pillars=None, dirichlet_surfaces=None, ceiling=ceiling
    )
    cost = mpsadjoint.inverse.cost_function([u_model], [u_data], controls, geometry)

    assert math.isclose(cost, 0.0)
