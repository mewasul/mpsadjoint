import numpy as np
import dolfin as df
import dolfin_adjoint as da

from mpsadjoint import (
    set_fenics_parameters,
    load_mesh_h5,
    define_state_space,
    define_bcs,
    define_weak_form,
    solve_forward_problem,
    solve_inverse_problem,
)

from mpsadjoint.mesh_setup import Geometry
from mpsadjoint.inverse import (
    solve_pdeconstrained_optimization_problem,
    active_to_scaled,
    theta_to_scaled,
    cost_function,
)

def setup_mesh_funspaces():
    
    set_fenics_parameters()
    
    mesh = da.UnitCubeMesh(1, 1, 1)

    volumes = df.MeshFunction("size_t", mesh, 3, 0)
    volumes.array()[:] = 0    

    surface_ceiling = df.MeshFunction("size_t", mesh, 2)
    ceiling = df.CompiledSubDomain(f"near(x[2], 1, 1e-5) && on_boundary")
    ceiling.mark(surface_ceiling, 2)

    surface_floor = df.MeshFunction("size_t", mesh, 2, 0)
    floor = df.CompiledSubDomain(f"near(x[2], 0) && on_boundary")
    floor.mark(surface_floor, 1)

    geometry = Geometry(mesh, volumes, surface_floor, surface_ceiling)
    
    TH = define_state_space(geometry.mesh)
    bcs = define_bcs(geometry.dirichlet_surfaces, TH)

    return TH, bcs, geometry


def test_cost_function_cb():
    """

    Tests that eval_cb_checkpoint and cost_function
    values are equal (in the final iteration).

    """

    TH, bcs, geometry = setup_mesh_funspaces()

    active = da.Constant(0.01)
    theta = da.Constant(0.0)

    R, state = define_weak_form(TH, active, theta, da.Constant(1), da.Constant(0))
    solve_forward_problem(R, state, bcs)

    u_synthetic, _ = state.split()

    # initial guesses

    init_active = da.Constant(0.0)
    init_theta = da.Constant(0.0)
    init_state = da.Function(TH)
    num_iterations = 3

    # then consider the inverse problem

    (
        active_opt,
        theta_opt,
        states,
        tracked_quantities,
    ) = solve_pdeconstrained_optimization_problem(
        geometry,
        [u_synthetic],
        [init_active],
        init_theta,
        [init_state],
        num_iterations,
    )

    u_opt = states[0].split()[0]

    active_scaled = active_to_scaled(active_opt[0])
    theta_scaled = theta_to_scaled(theta_opt)
    controls = [active_scaled, theta_scaled]

    cost_fun_value = cost_function([u_opt], [u_synthetic], controls, geometry)
    assert np.isclose(
        cost_fun_value, tracked_quantities[0][-1]
    ), "Error: Mismatch between calculated cost function and final tracked cost function value"


if __name__ == "__main__":
    test_cost_function_cb()
