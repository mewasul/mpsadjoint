import numpy as np
import dolfin as df
import dolfin_adjoint as da

from mpsadjoint import (
    define_state_space,
    define_weak_form,
    solve_forward_problem,
    set_fenics_parameters,
)


def create_forward_problem_unit_cube(mesh, active, theta):
    set_fenics_parameters()

    TH = define_state_space(mesh)

    R, state = define_weak_form(
        TH,
        active,
        theta,
        da.Constant(1),
        da.Constant(0),
    )

    V_TH, _ = TH.split()
    xmin_bnd = "on_boundary && near(x[0], 0)"
    bcs = [da.DirichletBC(V_TH, da.Constant(np.zeros(3)), xmin_bnd)]

    solve_forward_problem(R, state, bcs)

    u, _ = state.split()
    return u


def errornorm(u, u_synthetic, mesh):
    norm = lambda f: da.assemble(df.inner(f, f) * df.dx(mesh))
    return norm(u - u_synthetic) / norm(da.Constant(1))


def test_taylor_unit_cube():
    N = 3
    mesh = da.UnitCubeMesh(N, N, N)

    V = df.VectorFunctionSpace(mesh, "CG", 2)
    U = df.FunctionSpace(mesh, "CG", 1)
    u_synthetic = da.Function(V)

    active_ctrl = da.Function(U)
    active_ctrl.interpolate(da.Constant(0.01))

    theta_ctrl = da.Function(U)
    theta_ctrl.interpolate(da.Constant(0.01))

    h = [da.Function(U), da.Function(U)]
    h[0].interpolate(da.Constant(0.01))
    h[1].interpolate(da.Constant(0.01))

    u = create_forward_problem_unit_cube(mesh, active_ctrl, theta_ctrl)

    J = errornorm(u, u_synthetic, mesh)

    reduced_functional = da.ReducedFunctional(
        J,
        [da.Control(active_ctrl), da.Control(theta_ctrl)],
    )

    results = da.taylor_to_dict(reduced_functional, [da.Function(U), da.Function(U)], h)

    assert min(results["R0"]["Rate"]) > 0.9
    assert min(results["R1"]["Rate"]) > 1.9
    assert min(results["R2"]["Rate"]) > 2.9


if __name__ == "__main__":
    test_taylor_unit_cube()
