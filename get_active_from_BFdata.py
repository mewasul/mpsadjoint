import os
import numpy as np
import dolfin as df
from argparse import ArgumentParser
import mps                            # depends on this for getting units

from mpsadjoint import (
    load_mesh_h5,
    solve_inverse_problem,
    mps_to_fenics,
    set_fenics_parameters,
    write_displacement_to_file,
)

set_fenics_parameters()

def parse_cl_arguments():
    parser = ArgumentParser()

    parser.add_argument(
        "--from_step",
        type=int,
        default=243,
        help="Starting time step",
        )
    parser.add_argument(
        "--to_step",
        type=int,
        default=244,
        help="Final time step",
        )

    parser.add_argument(
        "--step_length",
        type=int,
        default=1,
        help="Stride (consider every _ step)",
        )

    parser.add_argument(
        "--num_iterations",
        type=int,
        default=100,
        help="How many iterations to use solving the inverse problem",
        )

    parser.add_argument(
        "--dose",
        type=str,
        dose="",
        help="dose (folder)"
        )

    d = parser.parse_args()

    return d.from_step, d.to_step, d.step_length, d.num_iterations, d.dose


from_step, to_step, step_length, num_iterations, dose = parse_cl_arguments()

experiment_data = "experiments/PointH4A_ChannelBF_VC_Seq0018_displacement.npy"

# load data from mechanical analysis; specific for corresponding experiment
mps_data = np.load(displacement_file)
mps_info = {"um_per_pixel" : 1.3552}      # this might vary

# then get command line arguments

output_folder = f"inverse_bf/step_{from_step}_{to_step}_{num_iterations}_omecamtiv_{dose}_DG"

# load mesh; specific for a given design
mesh_file = os.path.join("meshes", f"chip_wo_anchors_0p25.h5")
geometry = load_mesh_h5(mesh_file)

u_data = mps_to_fenics(mps_data, mps_info, geometry.mesh, from_step, to_step)[::step_length]

filename_displacement = f"{output_folder}/displacement_original.xdmf"
V = df.VectorFunctionSpace(geometry.mesh, "CG", 1)
write_displacement_to_file(V, u_data, filename_displacement)

active_str, theta = solve_inverse_problem(
    geometry,
    u_data,
    output_folder,
    num_iterations,
)
