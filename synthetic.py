import os
from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt
import dolfin_adjoint as da

from mpsadjoint import load_mesh_h5
from synthetic_framework import inverse_crime


def parse_cl_arguments():
    parser = ArgumentParser()

    parser.add_argument(
        "--from_step",
        type=int,
        default=140,
        help="Starting time step (out of 999)",
    )
    parser.add_argument(
        "--to_step",
        type=int,
        default=150,
        help="Final time step (out of 999)",
    )
    parser.add_argument(
        "--step_length",
        type=int,
        default=10,
        help="Stride (consider every _ step)",
    )

    parser.add_argument(
        "--active",
        type=str,
        default="sines",
        help="kind of active strain ('sines' or 'constant') to explore",
    )

    parser.add_argument(
        "--fiber",
        type=str,
        default="sines",
        help="kind of fiber direction ('sines' or 'constant') to explore",
    )

    parser.add_argument(
        "--noise_level",
        type=float,
        default=0.0,
        help="Noise level; how much noise we perturbate the synthetic data with",
    )

    parser.add_argument(
        "--num_iterations_iterative",
        type=int,
        default=100,
        help="How many iterations to use solving the inverse problem, in the iterative phase",
    )

    parser.add_argument(
        "--num_iterations_combined",
        type=int,
        default=100,
        help="How many iterations to use solving the inverse problem, in the combined phase",
    )

    parser.add_argument(
        "--mesh_resolution",
        type=str,
        default="0p5",
        help="Which mesh to use; given as charactheristic length (like 1p0, 0p5, ...)",
    )
    
    parser.add_argument(
        "--reg_factor",
        type=int,
        default=10000,
        help="Reg. factor",
    )

    d = parser.parse_args()

    assert d.active in [
        "sines",
        "constant",
    ], "Error: Unknown kind of active strain distribution."
    assert d.fiber in [
        "sines",
        "constant",
    ], "Error: Unknown kind of fiber direction distribution."
    assert d.from_step < d.to_step, "Error: 'from_step' needs to come before 'to_step'"
    assert 0 <= d.from_step < 999, "Error: 'from_step' out of range."
    assert 0 < d.to_step < 1000, "Error: 'to_step' out of range."
    assert d.noise_level >= 0.0, "Error: Noise level should be a positive number."

    return (
        d.from_step,
        d.to_step,
        d.step_length,
        d.active,
        d.fiber,
        d.noise_level,
        d.num_iterations_iterative,
        d.num_iterations_combined,
        d.mesh_resolution,
        d.reg_factor,
    )


def synthetic_experiment():
    (
        start,
        stop,
        step_length,
        active,
        fiber,
        noise_level,
        num_iterations_iterative,
        num_iterations_combined,
        mesh_res,
        reg_factor,
    ) = parse_cl_arguments()

    mesh_file = os.path.join("meshes", f"chip_omecamtiv_{mesh_res}_with_pillars.h5")
    geometry = load_mesh_h5(mesh_file)

    if fiber == "sines":
        theta = da.Expression("0.5*(sin((x[0] + x[1] + 210)/60))", degree=4)
    else:
        theta = da.Constant(0.5)

    active_over_time = []

    # * 0.25 to get to a comparable level with BF data in the sample chip
    scaling = 0.25 * np.load("mpsadjoint/active_stress.npy")[start:stop][::step_length]

    for t in scaling:
        if active == "sines":
            a = da.Expression("t*(1 + sin((x[0] + x[1] + 210)/60))", t=t, degree=4)
        else:
            a = da.Constant(t)
        active_over_time.append(a)

    output_folder = (
        f"inverse_crime_noise/inverse_volume_{mesh_res}_{start}_{stop}_{step_length}"
        + f"_{active}_{fiber}_{noise_level}_{num_iterations_iterative}_{num_iterations_combined}_{reg_factor}"
    )

    inverse_crime(
        geometry,
        active_over_time,
        theta,
        output_folder,
        num_iterations_iterative,
        num_iterations_combined,
        reg_factor,
        noise_level=noise_level,
    )


if __name__ == "__main__":
    synthetic_experiment()
